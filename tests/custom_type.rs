use test4a::{Equal, Message, PanicWhen, Runner};

// Definition of the type to test.
struct TextBox {
    text: String,
}

impl TextBox {
    fn new(default_value: &str) -> Self {
        Self {
            text: default_value.into(),
        }
    }

    fn empty(&self) -> bool {
        self.text.is_empty()
    }

    fn line_count(&self) -> usize {
        self.text.matches('\n').count() + 1
    }

    fn text(&self) -> &str {
        &self.text
    }

    fn add_line(&mut self, line: &str) {
        if line.matches('\n').count() > 0 {
            panic!("A line shouldn't contain the '\n' character.");
        }
        self.text += "\n";
        self.text += line;
    }

    fn clear(&mut self) {
        self.text = String::new();
    }
}

// Definition of constants for tests.
const NOT_EMPTY_LINE: &str = "Content";
const MULTIPLE_LINES: &str = "Line 1\nLine 2";

// Definition of the structure that contains expected values.
struct Expected {
    empty: bool,
    line_count: usize,
    text: String,
}

// Definition of the "act" methods.
fn clear(message: &mut Message, value: &mut TextBox) {
    message.set("Empty");
    value.clear();
}

fn add_empty_line(message: &mut Message, value: &mut TextBox) {
    message.set("Add empty line");
    value.add_line("");
}

fn add_non_empty_line(message: &mut Message, value: &mut TextBox) {
    message.set("Add non empty line");
    value.add_line(NOT_EMPTY_LINE);
}

fn add_multiple_lines(message: &mut Message, value: &mut TextBox) {
    message.set("Add multiple lines");
    value.add_line(MULTIPLE_LINES);
}

// Definition of the "assert" methods.
fn expect_empty(
    message: &mut Message,
    value: TextBox,
    expected: Expected,
) -> Equal<bool> {
    message.set("Text is empty or not");
    Equal::new(expected.empty, value.empty())
}

fn expect_line_count(
    message: &mut Message,
    value: TextBox,
    expected: Expected,
) -> Equal<usize> {
    message.set("Line count is correct");
    Equal::new(expected.line_count, value.line_count())
}

fn expect_text(
    message: &mut Message,
    value: TextBox,
    expected: Expected,
) -> Equal<String> {
    message.set("Text is correct");
    Equal::new(expected.text, value.text().into())
}

// Definition of the tests to execute.
#[test]
fn test_empty() {
    Runner::arrange(|message| {
        message.set("Empty text");
        TextBox::new("")
    })
    .act(clear, || Expected {
        empty: true,
        line_count: 1,
        text: "".into(),
    })
    .act(add_empty_line, || Expected {
        empty: false,
        line_count: 2,
        text: "\n".into(),
    })
    .act(add_non_empty_line, || Expected {
        empty: false,
        line_count: 2,
        text: "\n".to_string() + NOT_EMPTY_LINE,
    })
    .act_panic(PanicWhen::DebugAndRelease, add_multiple_lines)
    .assert(expect_empty)
    .assert(expect_line_count)
    .assert(expect_text);
}

#[test]
fn test_one_line() {
    Runner::arrange(|message| {
        message.set("Text with one line");
        TextBox::new(NOT_EMPTY_LINE)
    })
    .act(clear, || Expected {
        empty: true,
        line_count: 1,
        text: "".into(),
    })
    .act(add_empty_line, || Expected {
        empty: false,
        line_count: 2,
        text: NOT_EMPTY_LINE.to_string() + "\n",
    })
    .act(add_non_empty_line, || Expected {
        empty: false,
        line_count: 2,
        text: NOT_EMPTY_LINE.to_string() + "\n" + NOT_EMPTY_LINE,
    })
    .act_panic(PanicWhen::DebugAndRelease, add_multiple_lines)
    .assert(expect_empty)
    .assert(expect_line_count)
    .assert(expect_text);
}

#[test]
fn test_multiple_lines() {
    Runner::arrange(|message| {
        message.set("Text with multiple lines");
        TextBox::new(MULTIPLE_LINES)
    })
    .act(clear, || Expected {
        empty: true,
        line_count: 1,
        text: "".into(),
    })
    .act(add_empty_line, || Expected {
        empty: false,
        line_count: 3,
        text: MULTIPLE_LINES.to_string() + "\n",
    })
    .act(add_non_empty_line, || Expected {
        empty: false,
        line_count: 3,
        text: MULTIPLE_LINES.to_string() + "\n" + NOT_EMPTY_LINE,
    })
    .act_panic(PanicWhen::DebugAndRelease, add_multiple_lines)
    .assert(expect_empty)
    .assert(expect_line_count)
    .assert(expect_text);
}
