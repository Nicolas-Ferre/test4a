use test4a::{Equal, Message, PanicWhen, Runner};

// Definition of the structure that contains expected values.
struct Expected {
    value: usize,
}

// Definition of the "act" methods.
fn add_0(message: &mut Message, value: &mut usize) {
    message.set("Add 0 to the initial integer");
    *value += 0;
}

fn add_1(message: &mut Message, value: &mut usize) {
    message.set("Add 1 to the initial integer");
    *value += 1;
}

fn subtract_1(message: &mut Message, value: &mut usize) {
    message.set("Subtract 1 to the initial integer");
    *value -= 1;
}

// Definition of the "assert" methods.
fn expect_value(
    message: &mut Message,
    value: usize,
    expected: Expected,
) -> Equal<usize> {
    message.set("value == expected.value");
    Equal::new(value, expected.value)
}

// Definition of the tests to execute.
#[test]
fn test_zero() {
    Runner::arrange(|message| {
        message.set("Initial value of 0");
        0
    })
    .act(add_0, || Expected { value: 0 })
    .act(add_1, || Expected { value: 1 })
    .act_panic(PanicWhen::Debug, subtract_1)
    .assert(expect_value);
}

#[test]
fn test_other() {
    Runner::arrange(|message| {
        message.set("Initial value of 42");
        42
    })
    .act(add_0, || Expected { value: 42 })
    .act(add_1, || Expected { value: 43 })
    .act(subtract_1, || Expected { value: 41 })
    .assert(expect_value);
}
