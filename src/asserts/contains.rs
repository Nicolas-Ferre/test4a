//! Defines the type `Contains`.

use crate::Assert;
use std::fmt::Debug;

/// Asserts that a value is in a vector.
///
/// # Examples
///
/// ```rust
/// use test4a::{Assert, Contains};
///
/// let assert = Contains::new(vec![10, 1, 42], 1);
/// assert!(assert.success());
/// ```
pub struct Contains<T: PartialEq + Debug> {
    vector: Vec<T>,
    value: T,
}

impl<T: PartialEq + Debug> Contains<T> {
    /// Constructor.
    pub fn new(vector: Vec<T>, value: T) -> Self {
        Self { vector, value }
    }
}

impl<T: PartialEq + Debug> Assert for Contains<T> {
    fn success(&self) -> bool {
        self.vector.contains(&self.value)
    }

    fn error_message(&self) -> String {
        "Assert `vector.contains(value)` has failed with\n".to_string()
            + &format!("    vector: `{:?}`\n", self.vector)
            + &format!("    value: `{:?}`", self.value)
    }
}

#[cfg(test)]
mod tests {
    use crate::asserts::assert::Assert;
    use crate::Contains;

    #[test]
    fn test_empty_vector() {
        let assert = Contains::new(Vec::new(), 0);
        assert!(!assert.success())
    }

    #[test]
    fn test_not_in_vector() {
        let assert = Contains::new(vec![0, 1, 2, 3], 4);
        assert!(!assert.success())
    }

    #[test]
    fn test_in_vector() {
        let assert = Contains::new(vec![0, 1, 2, 3], 2);
        assert!(assert.success())
    }
}
