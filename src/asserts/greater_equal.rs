//! Defines the type `GreaterEqual`.

use crate::Assert;
use std::fmt::Debug;

/// Asserts that a value is greater than or equal to another one.
///
/// # Examples
///
/// ```rust
/// use test4a::{Assert, GreaterEqual};
///
/// let assert = GreaterEqual::new(43, 42);
/// assert!(assert.success());
/// ```
pub struct GreaterEqual<T: PartialOrd + Debug> {
    left: T,
    right: T,
}

impl<T: PartialOrd + Debug> GreaterEqual<T> {
    /// Constructor.
    pub fn new(left: T, right: T) -> Self {
        Self { left, right }
    }
}

impl<T: PartialOrd + Debug> Assert for GreaterEqual<T> {
    fn success(&self) -> bool {
        self.left >= self.right
    }

    fn error_message(&self) -> String {
        "Assert `left >= right` has failed with\n".to_string()
            + &format!("    left:  `{:?}`\n", self.left)
            + &format!("    right: `{:?}`", self.right)
    }
}

#[cfg(test)]
mod tests {
    use crate::asserts::assert::Assert;
    use crate::GreaterEqual;

    #[test]
    fn test_greater() {
        let assert = GreaterEqual::new(7, 2);
        assert!(assert.success())
    }

    #[test]
    fn test_equal() {
        let assert = GreaterEqual::new(7, 7);
        assert!(assert.success())
    }

    #[test]
    fn test_less() {
        let assert = GreaterEqual::new(5, 10);
        assert!(!assert.success())
    }
}
