//! Defines the type `False`.

use crate::Assert;
use std::convert::Into;

/// Asserts that a value is `false`.
///
/// # Examples
///
/// ```rust
/// use test4a::{Assert, False};
///
/// let assert = False::new(false);
/// assert!(assert.success());
/// ```
pub struct False {
    value: bool,
}

impl False {
    /// Constructor.
    pub const fn new(value: bool) -> Self {
        Self { value }
    }
}

impl Assert for False {
    fn success(&self) -> bool {
        !self.value
    }

    fn error_message(&self) -> String {
        "Assert has failed".into()
    }
}

#[cfg(test)]
mod tests {
    use crate::asserts::assert::Assert;
    use crate::False;

    #[test]
    fn test_true() {
        let assert = False::new(true);
        assert!(!assert.success())
    }

    #[test]
    fn test_false() {
        let assert = False::new(false);
        assert!(assert.success())
    }
}
