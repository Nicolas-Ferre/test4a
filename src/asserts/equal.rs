//! Defines the type `Equal`.

use crate::Assert;
use std::fmt::Debug;

/// Asserts that two values are equal.
///
/// # Examples
///
/// ```rust
/// use test4a::{Assert, Equal};
///
/// let assert = Equal::new(42, 42);
/// assert!(assert.success());
/// ```
pub struct Equal<T: PartialEq + Debug> {
    left: T,
    right: T,
}

impl<T: PartialEq + Debug> Equal<T> {
    /// Constructor.
    pub fn new(left: T, right: T) -> Self {
        Self { left, right }
    }
}

impl<T: PartialEq + Debug> Assert for Equal<T> {
    fn success(&self) -> bool {
        self.left == self.right
    }

    fn error_message(&self) -> String {
        "Assert `left == right` has failed with\n".to_string()
            + &format!("    left:  `{:?}`\n", self.left)
            + &format!("    right: `{:?}`", self.right)
    }
}

#[cfg(test)]
mod tests {
    use crate::asserts::assert::Assert;
    use crate::Equal;

    #[test]
    fn test_equal() {
        let assert = Equal::new(42, 42);
        assert!(assert.success())
    }

    #[test]
    fn test_not_equal() {
        let assert = Equal::new(1, 42);
        assert!(!assert.success())
    }
}
