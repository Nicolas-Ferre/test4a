//! Defines the type `LessEqual`.

use crate::Assert;
use std::fmt::Debug;

/// Asserts that a value is less than or equal to another one.
///
/// # Examples
///
/// ```rust
/// use test4a::{Assert, LessEqual};
///
/// let assert = LessEqual::new(42, 43);
/// assert!(assert.success());
/// ```
pub struct LessEqual<T: PartialOrd + Debug> {
    left: T,
    right: T,
}

impl<T: PartialOrd + Debug> LessEqual<T> {
    /// Constructor.
    pub fn new(left: T, right: T) -> Self {
        Self { left, right }
    }
}

impl<T: PartialOrd + Debug> Assert for LessEqual<T> {
    fn success(&self) -> bool {
        self.left <= self.right
    }

    fn error_message(&self) -> String {
        "Assert `left <= right` has failed with\n".to_string()
            + &format!("    left:  `{:?}`\n", self.left)
            + &format!("    right: `{:?}`", self.right)
    }
}

#[cfg(test)]
mod tests {
    use crate::asserts::assert::Assert;
    use crate::LessEqual;

    #[test]
    fn test_greater() {
        let assert = LessEqual::new(7, 2);
        assert!(!assert.success())
    }

    #[test]
    fn test_equal() {
        let assert = LessEqual::new(7, 7);
        assert!(assert.success())
    }

    #[test]
    fn test_less() {
        let assert = LessEqual::new(5, 10);
        assert!(assert.success())
    }
}
