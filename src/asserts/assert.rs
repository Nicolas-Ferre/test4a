//! Defines the trait `Assert`.

/// Defines an assertion.
///
/// See implementations provided with this library to know how to correctly
/// create your own asserts.
pub trait Assert {
    /// Tell whether the assertion has succeed.
    fn success(&self) -> bool;

    /// Return the error to print in case of failure.
    fn error_message(&self) -> String;
}
