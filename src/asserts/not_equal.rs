//! Defines the type `NotEqual`.

use crate::Assert;
use std::fmt::Debug;

/// Asserts that two values are different.
///
/// # Examples
///
/// ```rust
/// use test4a::{Assert, NotEqual};
///
/// let assert = NotEqual::new(43, 42);
/// assert!(assert.success());
/// ```
pub struct NotEqual<T: PartialEq + Debug> {
    left: T,
    right: T,
}

impl<T: PartialEq + Debug> NotEqual<T> {
    /// Constructor.
    pub fn new(left: T, right: T) -> Self {
        Self { left, right }
    }
}

impl<T: PartialEq + Debug> Assert for NotEqual<T> {
    fn success(&self) -> bool {
        self.left != self.right
    }

    fn error_message(&self) -> String {
        "Assert `left != right` has failed with\n".to_string()
            + &format!("    left:  `{:?}`\n", self.left)
            + &format!("    right: `{:?}`", self.right)
    }
}

#[cfg(test)]
mod tests {
    use crate::asserts::assert::Assert;
    use crate::NotEqual;

    #[test]
    fn test_equal() {
        let assert = NotEqual::new(42, 42);
        assert!(!assert.success())
    }

    #[test]
    fn test_not_equal() {
        let assert = NotEqual::new(1, 42);
        assert!(assert.success())
    }
}
