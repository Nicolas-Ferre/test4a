//! Defines the type `Greater`.

use crate::Assert;
use std::fmt::Debug;

/// Asserts that a value is greater than another one.
///
/// # Examples
///
/// ```rust
/// use test4a::{Assert, Greater};
///
/// let assert = Greater::new(43, 42);
/// assert!(assert.success());
/// ```
pub struct Greater<T: PartialOrd + Debug> {
    left: T,
    right: T,
}

impl<T: PartialOrd + Debug> Greater<T> {
    /// Constructor.
    pub fn new(left: T, right: T) -> Self {
        Self { left, right }
    }
}

impl<T: PartialOrd + Debug> Assert for Greater<T> {
    fn success(&self) -> bool {
        self.left > self.right
    }

    fn error_message(&self) -> String {
        "Assert `left > right` has failed with\n".to_string()
            + &format!("    left:  `{:?}`\n", self.left)
            + &format!("    right: `{:?}`", self.right)
    }
}

#[cfg(test)]
mod tests {
    use crate::asserts::assert::Assert;
    use crate::Greater;

    #[test]
    fn test_greater() {
        let assert = Greater::new(7, 2);
        assert!(assert.success())
    }

    #[test]
    fn test_equal() {
        let assert = Greater::new(7, 7);
        assert!(!assert.success())
    }

    #[test]
    fn test_less() {
        let assert = Greater::new(5, 10);
        assert!(!assert.success())
    }
}
