//! Defines the type `Multiple`.

use crate::Assert;

const DEFAULT_ERROR: &str = "-";

/// Asserts that multiple assertions created from an iterator have succeed.
///
/// # Examples
///
/// ```rust
/// use test4a::{Assert, Less, Multiple};
///
/// let assert = Multiple::new((0..10).map(|i| Less::new(i, 42)));
/// assert!(assert.success());
/// ```
pub struct Multiple<T: Assert> {
    asserts: Vec<T>,
}

impl<T: Assert> Multiple<T> {
    /// Constructor.
    pub fn new(iterator: impl Iterator<Item = T>) -> Self {
        Self {
            asserts: iterator.collect(),
        }
    }
}

impl<T: Assert> Assert for Multiple<T> {
    fn success(&self) -> bool {
        self.asserts.iter().all(Assert::success)
    }

    fn error_message(&self) -> String {
        let index = self.asserts.iter().position(|assert| !assert.success());
        if let Some(failed_assert_index) = index {
            self.asserts[failed_assert_index].error_message()
        } else {
            DEFAULT_ERROR.into()
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::asserts::assert::Assert;
    use crate::{Equal, Multiple, NotEqual};

    #[test]
    fn test_all_ok() {
        let assert = Multiple::new((0..10).map(|i| NotEqual::new(i, 11)));
        assert!(assert.success())
    }

    #[test]
    fn test_one_fail() {
        let assert = Multiple::new((0..10).map(|i| NotEqual::new(i, 7)));
        assert!(!assert.success())
    }

    #[test]
    fn test_all_fail() {
        let assert = Multiple::new((0..10).map(|i| Equal::new(i, 11)));
        assert!(!assert.success())
    }
}
