//! Defines the type `Less`.

use crate::Assert;
use std::fmt::Debug;

/// Asserts that a value is less than another one.
///
/// # Examples
///
/// ```rust
/// use test4a::{Assert, Less};
///
/// let assert = Less::new(42, 43);
/// assert!(assert.success());
/// ```
pub struct Less<T: PartialOrd + Debug> {
    left: T,
    right: T,
}

impl<T: PartialOrd + Debug> Less<T> {
    /// Constructor.
    pub fn new(left: T, right: T) -> Self {
        Self { left, right }
    }
}

impl<T: PartialOrd + Debug> Assert for Less<T> {
    fn success(&self) -> bool {
        self.left < self.right
    }

    fn error_message(&self) -> String {
        "Assert `left < right` has failed with\n".to_string()
            + &format!("    left:  `{:?}`\n", self.left)
            + &format!("    right: `{:?}`", self.right)
    }
}

#[cfg(test)]
mod tests {
    use crate::asserts::assert::Assert;
    use crate::Less;

    #[test]
    fn test_greater() {
        let assert = Less::new(7, 2);
        assert!(!assert.success())
    }

    #[test]
    fn test_equal() {
        let assert = Less::new(7, 7);
        assert!(!assert.success())
    }

    #[test]
    fn test_less() {
        let assert = Less::new(5, 10);
        assert!(assert.success())
    }
}
