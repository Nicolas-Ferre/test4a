//! Defines the type `True`.

use crate::Assert;

/// Asserts that a value is `true`.
///
/// # Examples
///
/// ```rust
/// use test4a::{Assert, True};
///
/// let assert = True::new(true);
/// assert!(assert.success());
/// ```
pub struct True {
    value: bool,
}

impl True {
    /// Constructor.
    pub const fn new(value: bool) -> Self {
        Self { value }
    }
}

impl Assert for True {
    fn success(&self) -> bool {
        self.value
    }

    fn error_message(&self) -> String {
        "Assert has failed".into()
    }
}

#[cfg(test)]
mod tests {
    use crate::asserts::assert::Assert;
    use crate::True;

    #[test]
    fn test_true() {
        let assert = True::new(true);
        assert!(assert.success())
    }

    #[test]
    fn test_false() {
        let assert = True::new(false);
        assert!(!assert.success())
    }
}
