//! Defines the enum `MessageType`.

/// Defines the different steps of a test.
#[derive(Clone, Copy)]
pub enum MessageType {
    Arrange,
    Act,
    Assert,
}
