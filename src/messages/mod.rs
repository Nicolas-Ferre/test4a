//! Defines tools to handle messages.

mod message;
mod message_type;

pub use self::message::Message;
pub use self::message_type::MessageType;
