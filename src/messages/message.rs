//! Defines the type `Message`.

use crate::messages::message_type::MessageType;

const DEFAULT_MESSAGE: &str = "-";

/// Stores messages for successive steps of a test.
///
/// # Examples
///
/// ```rust
/// use test4a::Message;
///
/// fn do_something(message: &mut Message) {
///     message.set("Execution of X");
///     // Execute X
/// }
/// ```
pub struct Message<'a> {
    current_type: MessageType,
    arrange_message: &'a str,
    act_message: &'a str,
    assert_message: &'a str,
}

impl<'a> Message<'a> {
    /// Constructor.
    pub(crate) const fn new() -> Self {
        Self {
            current_type: MessageType::Arrange,
            arrange_message: DEFAULT_MESSAGE,
            act_message: DEFAULT_MESSAGE,
            assert_message: DEFAULT_MESSAGE,
        }
    }

    /// Set the message for the current step of the test.
    pub fn set(&mut self, message: &'a str) {
        match self.current_type {
            MessageType::Arrange => self.arrange_message = message,
            MessageType::Act => self.act_message = message,
            MessageType::Assert => {
                self.assert_message = message;
            }
        }
    }

    /// Get the message corresponding to the Arrange step.
    pub(crate) const fn arrange_message(&self) -> &'a str {
        self.arrange_message
    }

    /// Get the message corresponding to the Act step.
    pub(crate) const fn act_message(&self) -> &'a str {
        self.act_message
    }

    /// Get the message corresponding to the Assert step.
    pub(crate) const fn assert_message(&self) -> &'a str {
        self.assert_message
    }

    /// Set the current step of the test.
    pub(crate) fn set_current_type(&mut self, current_type: MessageType) {
        self.current_type = current_type;
    }
}

#[cfg(test)]
mod tests {
    use crate::messages::message::DEFAULT_MESSAGE;
    use crate::messages::MessageType;
    use crate::Message;

    #[test]
    fn test_new() {
        let message = Message::new();
        assert_eq!(message.arrange_message(), DEFAULT_MESSAGE);
        assert_eq!(message.act_message(), DEFAULT_MESSAGE);
        assert_eq!(message.assert_message(), DEFAULT_MESSAGE);
    }

    #[test]
    fn test_arrange() {
        let mut message = Message::new();
        let arrange_message = "Arrange message example";
        message.set(arrange_message);
        assert_eq!(message.arrange_message(), arrange_message);
        assert_eq!(message.act_message(), DEFAULT_MESSAGE);
        assert_eq!(message.assert_message(), DEFAULT_MESSAGE);
    }

    #[test]
    fn test_act() {
        let mut message = Message::new();
        let act_message = "Act message example";
        message.set_current_type(MessageType::Act);
        message.set(act_message);
        assert_eq!(message.arrange_message(), DEFAULT_MESSAGE);
        assert_eq!(message.act_message(), act_message);
        assert_eq!(message.assert_message(), DEFAULT_MESSAGE);
    }

    #[test]
    fn test_assert() {
        let mut message = Message::new();
        let assert_message = "Assert message example";
        message.set_current_type(MessageType::Assert);
        message.set(assert_message);
        assert_eq!(message.arrange_message(), DEFAULT_MESSAGE);
        assert_eq!(message.act_message(), DEFAULT_MESSAGE);
        assert_eq!(message.assert_message(), assert_message);
    }

    #[test]
    fn test_all() {
        let mut message = Message::new();
        let arrange_message = "Arrange message example";
        let act_message = "Act message example";
        let assert_message = "Assert message example";
        message.set(arrange_message);
        message.set_current_type(MessageType::Act);
        message.set(act_message);
        message.set_current_type(MessageType::Assert);
        message.set(assert_message);
        assert_eq!(message.arrange_message(), arrange_message);
        assert_eq!(message.act_message(), act_message);
        assert_eq!(message.assert_message(), assert_message);
    }

    #[test]
    fn test_all_reversed() {
        let mut message = Message::new();
        let arrange_message = "Arrange message example";
        let act_message = "Act message example";
        let assert_message = "Assert message example";
        message.set_current_type(MessageType::Assert);
        message.set(assert_message);
        message.set_current_type(MessageType::Act);
        message.set(act_message);
        message.set_current_type(MessageType::Arrange);
        message.set(arrange_message);
        assert_eq!(message.arrange_message(), arrange_message);
        assert_eq!(message.act_message(), act_message);
        assert_eq!(message.assert_message(), assert_message);
    }
}
