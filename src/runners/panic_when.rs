//! Defines the enum `PanicWhen`.

/// Defines the configurations when a test can panic.
#[derive(Clone, Copy)]
pub enum PanicWhen {
    Debug,
    Release,
    DebugAndRelease,
}
