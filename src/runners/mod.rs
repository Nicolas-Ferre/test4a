//! Defines tools to describe and run tests.

mod act;
mod panic_when;
mod runner;

pub use self::panic_when::PanicWhen;
pub use self::runner::Runner;
