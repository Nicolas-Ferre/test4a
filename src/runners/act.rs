//! Defines the type `Act`.

use crate::Message;

/// Stores important information to run the Act step of a test.
pub struct Act<'a, T, E> {
    action_function: Box<Fn(&mut Message, &mut T) + 'a>,
    expectation_function: Box<Fn() -> E + 'a>,
}

impl<'a, T, E> Act<'a, T, E> {
    /// Constructor.
    ///
    /// `action_function` modifies the value to test, and `expectation_function`
    /// defines the values that are expected.
    pub fn new(
        action_function: impl Fn(&mut Message, &mut T) + 'a,
        expectation_function: impl Fn() -> E + 'a,
    ) -> Self {
        Self {
            action_function: Box::new(action_function),
            expectation_function: Box::new(expectation_function),
        }
    }

    /// Execute the step.
    pub fn execute(&self, message: &mut Message, value: &mut T) {
        (self.action_function)(message, value);
    }

    /// Retrieve expected values.
    pub fn expect(&self) -> E {
        (self.expectation_function)()
    }
}

#[cfg(test)]
mod tests {
    use crate::runners::act::Act;
    use crate::Message;

    const INITIAL_VALUE: usize = 0;
    const VALUE_EXAMPLE: usize = 10;
    const MESSAGE_EXAMPLE: &str = "Message";

    struct Expectation {
        value: usize,
    }

    fn action_example(message: &mut Message, value: &mut usize) {
        message.set(MESSAGE_EXAMPLE);
        *value = VALUE_EXAMPLE;
    }

    const fn expectation_example() -> Expectation {
        Expectation {
            value: VALUE_EXAMPLE,
        }
    }

    #[test]
    fn test_new() {
        let mut message = Message::new();
        let mut value = INITIAL_VALUE;
        let act = Act::new(action_example, expectation_example);
        act.execute(&mut message, &mut value);
        assert_eq!(message.arrange_message(), MESSAGE_EXAMPLE);
        assert_eq!(value, VALUE_EXAMPLE);
        let expected = act.expect();
        assert_eq!(expected.value, VALUE_EXAMPLE);
    }
}
